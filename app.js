var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
require('dotenv').config('./.env');
const { logger } = require('./config/logger')

var adminRouter = require('./api/admin/admin');
var userRouter = require('./api/users/users');

//database
const db = require('./config/database')
// Test DB
db.authenticate()
  .then(() => {
    logger.info('Database connected successfully');
    console.log("Database connected..")
  })
  .catch((err) => {
    console.log("Error: " + err)
    logger.error('Getting error when connecting database');
  })

var cors = require('cors')
var app = express();

app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/api/admin', adminRouter);
app.use('/api/user', userRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
