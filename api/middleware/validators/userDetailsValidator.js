const { body, header } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'changePassword': {
            return [
                body('oldPassword')
                    .exists().withMessage('oldPassword does not exists')
                    .notEmpty().withMessage('oldPassword is empty'),

                body('password')
                    .exists().withMessage('password does not exists')
                    .notEmpty().withMessage('password is empty')
                    .isLength({ min: 8 }).withMessage('password must be at least 8 chars long'),

                body('confirmPassword')
                    .exists().withMessage('confirmPassword does not exists')
                    .notEmpty().withMessage('confirmPassword is empty')
                    .custom((value, { req }) => {
                        if (value !== req.body.password) {
                            throw new Error('confirm password does not match password');
                        }
                        // Indicates the success of this synchronous custom validator
                        return true;
                    })
            ]
        }
        case 'addCardDetails': {
            return [
                body('firstName')
                    .exists().withMessage('firstName does not exists')
                    .notEmpty().withMessage('firstName is empty'),

                body('cardNumber')
                    .exists().withMessage('cardNumber does not exists')
                    .notEmpty().withMessage('cardNumber is empty'),

                body('expiryDate')
                    .exists().withMessage('expiryDate does not exists')
                    .notEmpty().withMessage('expiryDate is empty'),
                // isDate().withMessage('expiryDate must be date type'),

                body('zipCode')
                    .exists().withMessage('zipCode does not exists')
                    .notEmpty().withMessage('zipCode is empty'),

                body('country')
                    .exists().withMessage('country does not exists')
                    .notEmpty().withMessage('country is empty'),
            ]
        }
        case 'editCardDetails': {
            return [
                body('cardId')
                    .exists().withMessage('cardId does not exists')
                    .notEmpty().withMessage('cardId is empty'),

                body('firstName')
                    .exists().withMessage('firstName does not exists')
                    .notEmpty().withMessage('firstName is empty'),

                body('cardNumber')
                    .exists().withMessage('cardNumber does not exists')
                    .notEmpty().withMessage('cardNumber is empty'),

                body('expiryDate')
                    .exists().withMessage('expiryDate does not exists')
                    .notEmpty().withMessage('expiryDate is empty'),

                body('zipCode')
                    .exists().withMessage('zipCode does not exists')
                    .notEmpty().withMessage('zipCode is empty'),

                body('country')
                    .exists().withMessage('country does not exists')
                    .notEmpty().withMessage('country is empty'),
            ]
        }
    }
}