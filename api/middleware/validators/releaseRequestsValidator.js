const { body, header } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'ReleaseRequest': {
            return [
                body('songReleaseId')
                    .exists().withMessage('songReleaseId does not exists')
                    .notEmpty().withMessage('songReleaseId is empty')
            ]
        }
    }
}