const { body, header } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'buyShare': {
            return [
                body('songReleaseId')
                    .exists().withMessage('songReleaseId does not exists')
                    .notEmpty().withMessage('songReleaseId is empty'),
                body('cardId')
                    .exists().withMessage('cardId does not exists')
                    .notEmpty().withMessage('cardId is empty'),
                body('purchasedSalePercentage')
                    .exists().withMessage('purchasedSalePercentage does not exists')
                    .isNumeric().withMessage('purchasedSalePercentage must be number')
                    .notEmpty().withMessage('purchasedSalePercentage is empty'),
            ]
        }
    }
}