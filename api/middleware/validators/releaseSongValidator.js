const { body, header } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'releaseSong': {
            return [
                body('email')
                    .exists().withMessage('email does not exists')
                    .notEmpty().withMessage('email is empty')
                    .isEmail().withMessage('Invalid email'),
            ]
        }
        case 'songReleaseIdCheck': {
            return [
                body('songReleaseId')
                    .exists().withMessage('songReleaseId does not exists')
                    .notEmpty().withMessage('songReleaseId is empty')
            ]
        }
        case 'reportRoyalties': {
            return [
                body('songReleaseId')
                    .exists().withMessage('songReleaseId does not exists')
                    .notEmpty().withMessage('songReleaseId is empty'),
                body('reportRoyalties')
                    .exists().withMessage('reportRoyalties does not exists')
                    .notEmpty().withMessage('reportRoyalties is empty'),
            ]
        }
    }
}