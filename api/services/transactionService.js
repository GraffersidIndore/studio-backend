const { MoneyTransactions, SongReleases, Users } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');

exports.getAllTransactions = (userId) => {
    return new Promise((resolve, reject) => {
        MoneyTransactions.findAll({
            where: { "transaction_status": true },
            include: [
                {
                    model: Users,
                    as: "transactionUserDetails",
                    attributes: ["firstname", "lastname"],
                },
                {
                    model: SongReleases,
                    as: "transactionSongDetails",
                    attributes: ["song_name"],
                }],
            order: [["created_on", "DESC"]]
        }).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    })
}