const { Users } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');
const request = require('request-promise');

exports.registerUser = (body, hashedpassword) => {
    return new Promise((resolve, reject) => {
        Users.create({ 'email': body.email.toLowerCase(), 'firstname': body.firstname.toLowerCase(), 'lastname': body.lastname.toLowerCase(), 'password': hashedpassword, user_role: "user" })
            .then((result) => {
                resolve(result)
            }).catch((error) => reject(error))
    })
}

exports.login = (email, password) => {
    return new Promise((resolve, reject) => {
        Users.findOne({ attributes: ['user_id', 'user_role', 'email', 'password', 'firstname', 'lastname', 'profile_picture', 'is_verified', 'is_active'], where: { 'email': email.toLowerCase() } })
            .then(user => {
                resolve(user);
            }).catch((error) => reject(error))
    });
}

exports.socialSignIn = (body) => {
    return new Promise(async (resolve, reject) => {
        if (body.type === "facebook") {
            try {
                const options = {
                    method: 'GET',
                    uri: `https://graph.facebook.com/me`,
                    qs: {
                        access_token: body.access_token,
                        fields: 'first_name, middle_name, last_name, email, picture',
                    }
                };

                await request(options)
                    .then(async fbRes => {
                        const parsedRes = JSON.parse(fbRes);
                        let user = await Users.findOne({ attributes: ['user_id', 'email', 'password', 'firstname', 'lastname', 'profile_picture', 'is_verified', 'is_active'], where: { 'email': parsedRes.email.toLowerCase() } })
                        if (user !== null) {
                            resolve({ "user_id": user.user_id })
                        } else {
                            let user = await Users.create({ 'email': parsedRes.email.toLowerCase(), 'firstname': parsedRes.first_name, 'lastname': parsedRes.last_name, 'profile_picture': parsedRes.picture.data.url, user_role: "user", 'is_verified': true })
                            resolve({ "user_id": user.user_id })
                        }
                    })
            } catch (error) {
                reject(error)
            }
        } else if (body.type = "google") {
            try {
                const options = {
                    method: 'GET',
                    uri: `https://oauth2.googleapis.com/tokeninfo`,
                    qs: {
                        id_token: body.access_token,
                    }
                };

                await request(options)
                    .then(async googleRes => {
                        const parsedRes = JSON.parse(googleRes);
                        let user = await Users.findOne({ attributes: ['user_id', 'email', 'password', 'firstname', 'lastname', 'profile_picture', 'is_verified', 'is_active'], where: { 'email': parsedRes.email.toLowerCase() } })
                        if (user !== null) {
                            resolve({ "user_id": user.user_id })
                        } else {
                            let user = await Users.create({ 'email': parsedRes.email.toLowerCase(), 'firstname': parsedRes.given_name, 'lastname': parsedRes.family_name, 'profile_picture': parsedRes.picture, user_role: "user", 'is_verified': true })
                            resolve({ "user_id": user.user_id })
                        }
                    })
            } catch (error) {
                reject(error)
            }
        }

    });
}

exports.logout = (userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'access_token': "" }, { where: { 'user_id': userId } })
            .then(user => {
                resolve(user[1])
            }).catch((error) => reject(error))
    })
}

exports.setUserAccessToken = (token, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'access_token': token, "last_loggedin_at": new Date() }, {
            where: { 'user_id': userId },
            returning: true,
            plain: true
        }).then(async user => {
            let data = await Users.findOne({ attributes: ['email', 'firstname', 'lastname', 'profile_picture', 'access_token'], where: { 'user_id': userId } })
            resolve(data)
        }).catch((error) => reject(error))
    });
}

exports.verifyAccount = (token) => {
    return new Promise((resolve, reject) => {
        Users.findOne({
            where: { 'access_token': token }
        }).then(async user => {
            if (user !== null) {
                let data = await Users.update({ 'is_verified': true }, { where: { 'access_token': token } })
                resolve(data[1])
            } else {
                resolve(user)
            }
        }).catch((error) => reject(error))
    });
}

exports.setResetPasswordTokenAndTime = (token, tokenExpiryTime, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'reset_password_token': token, 'reset_password_expires': tokenExpiryTime, },
            { where: { 'user_id': userId } })
            .then(async user => {
                let data = await Users.findOne({ attributes: ['email', 'firstname', 'lastname', 'reset_password_token'], where: { 'user_id': userId } })
                resolve(data)
            }).catch((error) => reject(error))
    });
}

exports.checkTokenExpiryTime = (token, date) => {
    return new Promise((resolve, reject) => {
        Users.findOne({
            where: { 'reset_password_token': token, 'reset_password_expires': { [Op.gte]: date } },
            attributes: ["user_id"], raw: true
        }).then(user => {
            resolve(user)
        }).catch((error) => reject(error))
    })
}

exports.resetPassword = (hashedpassword, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'password': hashedpassword, 'reset_password_token': "", "last_password_updated_at": new Date() }, {
            where: { 'user_id': userId },
            returning: ['email'], plain: true
        }).then(user => {
            resolve(user[1])
        }).catch((error) => reject(error))
    });
}

exports.isEmailUnique = (email) => {
    return new Promise((resolve, reject) => {
        Users.findOne({ attributes: ['email'], where: { 'email': email.toLowerCase() } })
            .then(user => {
                if (user !== null) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            }).catch((error) => reject(error))
    });
}

exports.isUserExist = (email) => {
    return new Promise((resolve, reject) => {
        Users.findOne({ attributes: ['email', 'user_id'], where: { 'email': email.toLowerCase() } })
            .then(user => {
                resolve(user);
            }).catch((error) => reject(error))
    });
}

exports.setTokenExpiryTime = () => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        date.setMinutes(date.getMinutes() + 30);
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}


exports.getDate = () => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}