const { Users, CardDetails, UserShares } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');
const Sequelize = require('../../config/database');


exports.userDetails = (userId) => {
    return new Promise((resolve, reject) => {
        Users.findOne({
            where: { "user_id": userId },
            attributes: {
                exclude: ["password", "created_on", "access_token", "is_active",
                    "is_verified", "reset_password_expires", "reset_password_token",
                    "updated_on", "user_id", "user_role"]
            }
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.getOldPassword = (userId) => {
    return new Promise((resolve, reject) => {
        Users.findOne({
            where: { "user_id": userId },
            attributes: ['password']
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.editUserDetails = (userId, body) => {
    return new Promise((resolve, reject) => {
        Users.update(body, {
            where: { "user_id": userId }
        }).then((result) => {
            resolve(result[0])
        }).catch((error) => reject(error))
    })
}

exports.cardLists = (userId) => {
    return new Promise((resolve, reject) => {
        CardDetails.findAll({
            where: { "user_id": userId }
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.cardDetails = (userId, cardId) => {
    return new Promise((resolve, reject) => {
        CardDetails.findOne({
            where: { "user_id": userId, "card_id": cardId }
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.addCardDetails = (body) => {
    return new Promise((resolve, reject) => {
        CardDetails.create(body).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })

}
exports.editCardDetails = (userId, cardId, body) => {
    return new Promise((resolve, reject) => {
        CardDetails.update(body, {
            where: { "user_id": userId, "card_id": cardId }
        }).then((result) => {
            resolve(result[0])
        }).catch((error) => reject(error))
    })
}

exports.deleteAccount = (userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ "is_active": false }, {
            where: { "user_id": userId }
        }).then((result) => {
            resolve(result[0])
        }).catch((error) => reject(error))
    })
}

exports.getAllUsers = (body) => {
    return new Promise((resolve, reject) => {
        let whereStatement;
        if (body && body.email) {
            whereStatement = { "email": { [Op.like]: '%' + body.email + '%' }, "user_role": "user", "is_verified": true }
        } else {
            whereStatement = { "user_role": "user", "is_verified": true }
        }
        Users.findAll({
            where: whereStatement,
            include: [
                {
                    model: UserShares,
                    as: "userShares",
                    attributes: []
                }
            ],
            group: ['user_id'],
            attributes: {
                include: [[Sequelize.fn('COUNT', Sequelize.col('userShares.user_id')), 'ownedShareCount']],
                exclude: ["password", "created_on", "access_token", "is_active",
                    "is_verified", "reset_password_expires", "reset_password_token",
                    "updated_on", "user_role"]
            }
        }).then((result) => {
            resolve(result)
        }).catch((error) => {
            console.log(error);
            reject(error)
        })
    })
}
