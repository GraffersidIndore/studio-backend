const { SongReleases, Media, Users, Notificatons } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');
const Sequelize = require('../../config/database');


exports.getAllRequests = () => {
    return new Promise(async (resolve, reject) => {
        let releaseRequestCount = await SongReleases.count({ where: { "release_approved": false, "is_archived": false, "is_active": true } })
        SongReleases.findAll({
            where: { "release_approved": false, "is_archived": false, "is_active": true },
            include: [
                {
                    model: Users,
                    as: "SongReleaseUserDetails",
                    attributes: ['email', 'firstname', 'lastname'],
                }
            ]
        }).then((result) => {
            resolve({ "releaseRequests": result, "releaseRequestCount": releaseRequestCount })
        }).catch((error) => reject(error))
    })
}

exports.approveRequest = (body) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        let songDetails = await SongReleases.findOne({
            where: { "song_release_id": body.songReleaseId },
            attributes: ["user_id", "song_release_id", "song_name"]
        });

        let notificationBody = {
            user_id: songDetails.user_id,
            song_release_id: songDetails.song_release_id,
            notification_message: `Your release “${songDetails.song_name}” has been approved. Your sale is now active!`,
            notification_type: "release confirmed"
        }
        console.log(notificationBody)
        await Notificatons.create(notificationBody, { transaction: t });

        SongReleases.update({ "release_approved": true }, { where: { "song_release_id": body.songReleaseId }, transaction: t })
            .then(async (result) => {
                await t.commit();
                resolve(result)
            }).catch(async (error) => {
                await t.rollback();
                reject(error)
            })
    })
}

exports.rejectRequest = (body) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        let songDetails = await SongReleases.findOne({
            where: { "song_release_id": body.songReleaseId },
            attributes: ["user_id", "song_release_id", "song_name"]
        });

        let notificationBody = {
            user_id: songDetails.user_id,
            song_release_id: songDetails.song_release_id,
            notification_message: `Your release “${songDetails.song_name}” has been rejected.`,
            notification_type: "release rejected"
        }
        console.log(notificationBody)
        await Notificatons.create(notificationBody, { transaction: t });

        SongReleases.update({ "is_active": false }, { where: { "song_release_id": body.songReleaseId }, transaction: t })
            .then(async (result) => {
                await t.commit();
                resolve(result)
            }).catch(async (error) => {
                await t.rollback();
                reject(error)
            })
    })
}

exports.archiveRequest = (body) => {
    return new Promise((resolve, reject) => {
        SongReleases.update({ "is_archived": true }, { where: { "song_release_id": body.songReleaseId } })
            .then((result) => {
                resolve(result)
            }).catch((error) => reject(error))
    })
}

exports.getAllArchiveRequests = () => {
    return new Promise(async (resolve, reject) => {
        let archivedReleaseRequestCount = await SongReleases.count({ where: { "release_approved": false, "is_archived": true, "is_active": true } })
        SongReleases.findAll({
            where: { "release_approved": false, "is_archived": true, "is_active": true },
            include: [
                {
                    model: Users,
                    as: "SongReleaseUserDetails",
                    attributes: ['email', 'firstname', 'lastname'],
                }
            ]
        }).then((result) => {
            resolve({ "archivedReleaseRequests": result, "archivedReleaseRequestCount": archivedReleaseRequestCount })
        }).catch((error) => reject(error))
    })
}