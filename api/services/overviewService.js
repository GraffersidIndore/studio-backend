const { SongReleases, Media, Users, Notificatons } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');
const Sequelize = require('../../config/database');

exports.getOverview = (body) => {
    return new Promise((resolve, reject) => {

        let releaseRequestCount = SongReleases.count({ where: { "release_approved": false, "is_archived": false, "is_active": true } })
        let activeSalesCount = SongReleases.count({ where: { "release_approved": true, "is_active": true } })

        let TodaysNewSignedUpUsers = Users.count({ where: { "created_on": { [Op.gte]: getDate() }, "user_role": "user" } })
        let LastSevenDaysNewSignedUpUsers = Users.count({
            where: {
                "created_on": { [Op.between]: [addDays(7), getDate()] },
                "user_role": "user"
            }
        })
        let LastThirtyDaysNewSignedUpUsers = Users.count({
            where: {
                "created_on": { [Op.between]: [addDays(30), getDate()] },
                "user_role": "user"
            }
        })
        let LastOneYearNewSignedUpUsers = Users.count({
            where: {
                "created_on": { [Op.between]: [addDays(365), getDate()] },
                "user_role": "user"
            }
        })


        let TodaysDeletedAccounts = Users.count({
            where: { "updated_on": { [Op.gte]: getDate() }, "user_role": "user", "is_active": false }
        })

        let LastSevenDaysDeletedAccounts = Users.count({
            where: {
                "updated_on": { [Op.between]: [addDays(7), getDate()] },
                "user_role": "user", "is_active": false
            }
        })

        let LastThirtyDaysDeletedAccounts = Users.count({
            where: {
                "updated_on": { [Op.between]: [addDays(30), getDate()] },
                "user_role": "user", "is_active": false
            }
        })
        let LastOneYearDeletedAccounts = Users.count({
            where: {
                "updated_on": { [Op.between]: [addDays(365), getDate()] },
                "user_role": "user", "is_active": false
            }
        })


        let activeLogins = Users.count({
            where: { "last_loggedin_at": { [Op.gte]: getDate() }, "user_role": "user", "access_token": { [Op.ne]: null } }
        })

        let TodaysLogins = Users.count({
            where: { "last_loggedin_at": { [Op.gte]: getDate() }, "user_role": "user" }
        })

        let LastSevenDaysLogins = Users.count({
            where: {
                "last_loggedin_at": { [Op.between]: [addDays(7), getDate()] },
                "user_role": "user"
            }
        })

        let LastThirtyDaysLogins = Users.count({
            where: {
                "last_loggedin_at": { [Op.between]: [addDays(30), getDate()] },
                "user_role": "user"
            }
        })
        let LastOneYearLogins = Users.count({
            where: {
                "last_loggedin_at": { [Op.between]: [addDays(365), getDate()] },
                "user_role": "user"
            }
        })

        Promise.all([
            TodaysNewSignedUpUsers,
            LastSevenDaysNewSignedUpUsers,
            LastThirtyDaysNewSignedUpUsers,
            LastOneYearNewSignedUpUsers,

            TodaysDeletedAccounts,
            LastSevenDaysDeletedAccounts,
            LastThirtyDaysDeletedAccounts,
            LastOneYearDeletedAccounts,

            releaseRequestCount,
            activeSalesCount,

            activeLogins,
            TodaysLogins,
            LastSevenDaysLogins,
            LastThirtyDaysLogins,
            LastOneYearLogins
        ])
            .then((values) => {
                let response = {
                    newUserSignedUp: {
                        "_24Hour": values[0],
                        "_7Days": values[1],
                        "_30Days": values[2],
                        "_365Days": values[3]
                    },
                    deletedAccounts: {
                        "_24Hour": values[4],
                        "_7Days": values[5],
                        "_30Days": values[6],
                        "_365Days": values[7]
                    },
                    releaseRequests: values[8],
                    activeSales: values[9],

                    logins: {
                        "activeLogins": values[10],
                        "_24Hour": values[11],
                        "_7Days": values[12],
                        "_30Days": values[13],
                        "_365Days": values[14]
                    },

                }
                resolve(response)

            }).catch((error) => {
                console.log(error)
                reject(error)
            })

    })
}

function getDate() {
    let date = new Date();
    return (moment(date).format("YYYY-MM-DD"));
}

function addDays(daysCount) {
    let date = new Date();
    date.setDate(date.getDate() - daysCount);
    return (moment(date).format("YYYY-MM-DD"));
}