const { SongReleases, UserShares, Users } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');
const Sequelize = require('../../config/database');

exports.releaseSong = (body) => {
    return new Promise((resolve, reject) => {
        SongReleases.create(body)
            .then((result) => {
                resolve(result)
            }).catch((error) => reject(error))
    })
}

exports.editSale = (userId, songReleaseId, body) => {
    return new Promise((resolve, reject) => {
        SongReleases.update(body, { where: { "song_release_id": songReleaseId, "user_id": userId } })
            .then((result) => {
                resolve(result[0])
            }).catch((error) => reject(error))
    })
}

exports.cancelRelease = (userId, songReleaseId) => {
    return new Promise((resolve, reject) => {
        SongReleases.update({ "is_active": false }, { where: { "song_release_id": songReleaseId, "user_id": userId } })
            .then((result) => {
                resolve(result[0])
            }).catch((error) => reject(error))
    })
}

exports.getAllReleasedSongs = (body) => {
    return new Promise((resolve, reject) => {
        let whereStatement;
        if (body && body.songName) {
            whereStatement = { "song_name": { [Op.like]: '%' + body.songName + '%' }, "release_approved": true, "is_active": true }
        } else {
            whereStatement = { "release_approved": true, "is_active": true }
        }

        SongReleases.findAll({
            where: whereStatement,
            attributes: ['song_release_id', 'song_cover_picture', 'song_name', 'artist_name', 'song_total_earning', 'royalties_updated_time',
                'song_release_date', 'sell_sale_percentage', 'sold_sale_percentage', 'sale_price_per_percent', 'is_sale_active'],
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.getSongDetails = (params) => {
    return new Promise((resolve, reject) => {
        SongReleases.findOne({
            where: { "song_release_id": params.songReleaseId }
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.userReleases = (userId) => {
    return new Promise((resolve, reject) => {
        SongReleases.findAll({
            where: { "user_id": userId, "is_active": true },
            include: [
                {
                    model: UserShares,
                    as: "songInvestors",
                    // attributes: [
                    //     [Sequelize.fn('sum', Sequelize.col('total_purchased_price')), 'total_raised_by_shares']
                    // ],
                    attributes: []
                }
            ],
            group: ['song_release_id'],
            attributes: {
                include: [[Sequelize.fn('COUNT', Sequelize.col('songInvestors.song_release_id')), 'investorCount'],
                [Sequelize.fn('sum', Sequelize.col('songInvestors.total_purchased_price')), 'total_raised_by_shares']]
            }

            // where: { "user_id": userId, "is_active": true },
            // include: [
            //     {
            //         model: UserShares,
            //         as: "songInvestors",
            //         // attributes: [],
            //     }],
            // group: ['song_release_id'],
            // attributes: {
            //     include: [[Sequelize.fn('COUNT', Sequelize.col('songInvestors.song_release_id')), 'investorCount']]
            // }
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.userShares = (userId) => {
    return new Promise((resolve, reject) => {
        UserShares.findAll({
            where: { "user_id": userId, "is_active": true },
            include: [
                {
                    model: SongReleases,
                    as: "songDetails",
                    attributes: ['song_cover_picture', 'artist_name', 'song_name', 'song_release_date']
                }
            ],
            attributes: ['purchased_percentage_sale', 'sale_exp_date', 'created_on'],
            order: [["created_on", "DESC"]]
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.detailedUserShares = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            var ReleasedSongs = await UserShares.findAll({
                where: { "user_id": userId, "is_active": true },
                include: [
                    {
                        model: SongReleases,
                        as: "songDetails",
                        attributes: ['song_name', 'song_cover_picture', 'audio_file', 'artist_name', 'artist_picture', 'song_release_date']
                    }
                ],
                attributes: ['song_release_id'],
                group: ['user_shares.song_release_id'],
            })

            let releasedSongs = JSON.parse(JSON.stringify(ReleasedSongs))
            await asyncForEach(releasedSongs, async function (releasedSong, index) {
                return new Promise(async (resolve, reject) => {
                    let share = await UserShares.findAll({
                        where: { "user_id": userId, "song_release_id": releasedSong.song_release_id },
                        attributes: ['user_shares_id', 'purchased_percentage_sale', 'created_on', 'sale_exp_date'],
                        order: [["created_on", "DESC"]]
                    })
                    releasedSongs[index].shares = share;
                    resolve();
                })
            })

            resolve(releasedSongs)
        } catch (error) {
            reject(error)
        }
    })
}

exports.songInvestors = (userId, params) => {
    return new Promise((resolve, reject) => {
        SongReleases.findOne({
            where: { "user_id": userId, "song_release_id": params.songReleaseId, "is_active": true },
            include: [
                {
                    model: UserShares,
                    as: "songInvestors",
                    attributes: ['song_release_id', 'purchased_percentage_sale', 'sale_exp_date', 'user_id', "created_on"],
                    include: [
                        {
                            model: Users,
                            as: "investorsDetail",
                            attributes: ['firstname', 'lastname', 'profile_picture'],
                        }
                    ]
                }
            ],
            attributes: [],

            order: [
                [Sequelize.literal('songInvestors.created_on'), 'DESC']
            ]

            // attributes: [
            //     [Sequelize.fn('COUNT', Sequelize.col('songInvestors.user_shares_id')), 'investorCount']
            // ],
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.userLatestInvestors = (userId, params) => {
    return new Promise(async (resolve, reject) => {
        let songReleaseIdArray = [];
        let songReleases = await SongReleases.findAll({
            where: { "user_id": userId, "release_approved": true, "is_active": true },
            attributes: ["song_release_id"]
        });
        await asyncForEach(songReleases, async function (songRelease, index) {
            return new Promise(async (resolve, reject) => {
                songReleaseIdArray.push(songRelease.song_release_id)
                resolve();
            })
        })
        UserShares.findAll({
            where: { "song_release_id": songReleaseIdArray },
            include: [
                {
                    model: Users,
                    as: "investorsDetail",
                    attributes: ['firstname', 'lastname', "profile_picture"]
                }
            ],
            attributes: ["total_purchased_price", 'created_on']
        }).then((result) => {
            resolve(result)
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })
}

exports.AddreleaseSongMedia = (body, songReleaseId) => {
    return new Promise((resolve, reject) => {
        SongReleases.update(body, { where: { "song_release_id": songReleaseId } })
            .then((result) => {
                resolve(result)
            }).catch((error) => reject(error))
    })
}

exports.reportRoyalties = (body) => {
    return new Promise(async (resolve, reject) => {

        const t = await Sequelize.transaction();
        let songdetails = await SongReleases.findOne({ where: { "song_release_id": body.songReleaseId }, attributes: ["song_total_earning", "user_id"] })
        let songTotalEarning = parseFloat(songdetails.song_total_earning) + parseFloat(body.reportRoyalties)
        let date = new Date();
        let reportRoyalties = parseFloat(body.reportRoyalties)
        let onePercentOfreportRoyalties = reportRoyalties / 100;

        //fetching users lists who has valid shares
        let userShares = await UserShares.findAll({ where: { "song_release_id": body.songReleaseId, "sale_exp_date": { [Op.gte]: getDate() } }, attributes: ["user_id", "sale_exp_date", "purchased_percentage_sale"] })
        var totalDistributedAmount = 0;
        await asyncForEach(userShares, async function (userShare, index) {
            return new Promise(async (resolve, reject) => {

                //fetching user current outstanding balance and all time income
                let user = await Users.findOne({
                    where: { "user_id": userShare.user_id }, transaction: t,
                    attributes: ["outstanding_balance", "all_time_income"]
                })
                let userCurrentOutstancdingBalance = parseFloat(user.outstanding_balance)
                let userCurrentAllTimeIncome = parseFloat(user.all_time_income)

                //updating the outstanding balance and all time income
                await Users.update({
                    outstanding_balance: userCurrentOutstancdingBalance + (onePercentOfreportRoyalties * userShare.purchased_percentage_sale),
                    all_time_income: userCurrentAllTimeIncome + (onePercentOfreportRoyalties * userShare.purchased_percentage_sale)
                }, {
                    where: { "user_id": userShare.user_id }, transaction: t
                })
                totalDistributedAmount = parseFloat(totalDistributedAmount) + (onePercentOfreportRoyalties * userShare.purchased_percentage_sale);
                resolve();
            })
        })

        // giving remaining share amount to the song owner
        let ownerShareAmount = reportRoyalties - totalDistributedAmount;
        let songOwnerDetails = await Users.findOne({
            where: { "user_id": songdetails.user_id }, transaction: t,
            attributes: ["outstanding_balance", "all_time_income"]
        })

        // Updating song owner outstanding balance and all time income
        let songOwnerCurrentOutstancdingBalance = parseFloat(songOwnerDetails.outstanding_balance)
        let songOwnerCurrentAllTimeIncome = parseFloat(songOwnerDetails.all_time_income)
        await Users.update({
            outstanding_balance: songOwnerCurrentOutstancdingBalance + ownerShareAmount,
            all_time_income: songOwnerCurrentAllTimeIncome + ownerShareAmount
        }, {
            where: { "user_id": songdetails.user_id }, transaction: t
        })

        //Updating song total earning and updated time 
        SongReleases.update({ "song_total_earning": songTotalEarning, "royalties_updated_time": date }, { where: { "song_release_id": body.songReleaseId }, transaction: t })
            .then(async (result) => {
                await t.commit();
                resolve(result[0])
            }).catch(async (error) => {
                await t.rollback();
                reject(error)
            })
    })
}


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

function getDate() {
    let date = new Date();
    return (moment(date).format("YYYY-MM-DD"));
}