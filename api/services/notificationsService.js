const { Notificatons } = require('../models');
const { Op } = require('sequelize')

exports.getUserNotifications = (userId) => {
    return new Promise(async (resolve, reject) => {
        Notificatons.findAll({
            where: { "user_id": userId },
        }).then((result) => {
            resolve(result)
        }).catch((error) => reject(error))
    })
}

exports.deleteNotification = (userId, body) => {
    return new Promise(async (resolve, reject) => {
        Notificatons.destroy({
            where: { "notification_id": body.notificationId, "user_id": userId },
        }).then((result) => {
            resolve(result[0])
        }).catch((error) => reject(error))
    })
}