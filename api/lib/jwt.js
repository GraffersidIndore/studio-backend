const jwt = require('jsonwebtoken');

exports.generateToken = async (id, role) => {
    return new Promise((resolve, reject) => {
        if (role) {
            const token = jwt.sign({ 'userId': id, "user_role": role }, process.env.SECRET);
            resolve(token);
        } else {
            const token = jwt.sign({ 'userId': id }, process.env.SECRET);
            resolve(token);
        }

    });
}

/**
 * Verify Token
 */
exports.verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        try {
            const decoded = jwt.verify(token, process.env.SECRET);
            if (decoded.userId) {
                resolve(decoded);
            } else {
                resolve(false);
            }
        } catch (error) {
            // console.log("error:   ", error)
            reject(error)
        }
    });
}