const bcrypt = require('bcrypt');

/**
 * Hash Password Method
 */
exports.hashPassword = (password) => {
    return new Promise((resolve) => {
        resolve(bcrypt.hashSync(password, bcrypt.genSaltSync(8)))
    })
}

/**
 * comparePassword
 */
exports.comparePassword = (hashPassword, password) => {
    return bcrypt.compareSync(password, hashPassword);
}