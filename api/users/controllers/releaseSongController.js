const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
var jwt = require('../../lib/jwt');
var path = require('path');
const fs = require('fs');

var formidable = require('formidable');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    region: "us-east-2",
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const releaseSongService = require('../../services/releaseSongService')

exports.releaseSong = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/release_song -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    var songDetails = {};
    var artistPicture;
    var audioFile;
    var songCoverPicture;
    var picture1, picture2, picture3, picture4, picture5, picture6;

    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    form.on('field', function (field, value) {
        if (field == 'artistName') {
            songDetails.artist_name = value;
        } else if (field == 'artistDescription') {
            songDetails.artist_description = value;
        } else if (field == 'songName') {
            songDetails.song_name = value;
        }
        else if (field == 'songReleaseDate') {
            songDetails.song_release_date = value;
        }
        else if (field == 'recordLabel') {
            songDetails.record_label = value;
        }
        else if (field == 'isSaleActive') {
            songDetails.is_sale_active = value;
        }
        else if (field == 'sellSalePercentage') {
            songDetails.sell_sale_percentage = value;
        }
        else if (field == 'salePricePerPercent') {
            songDetails.sale_price_per_percent = value;
        }
        else if (field == 'saleExpiryDate') {
            songDetails.sale_exp_date = value;
        }
        else if (field == 'saleDescription') {
            songDetails.sale_description = value;
        }

    })
    form.on('file', async function (field, file) {
        if (field == 'artistPicture') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: 'Artist picture should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                artistPicture = file;
            }
        }
        else if (field == 'audioFile') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "audio")) {
                        res.status(422).json({ status: false, message: 'Audio file should be audio.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                audioFile = file;
            }

        }
        else if (field == 'songCoverPicture') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: 'Song cover picture should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                songCoverPicture = file;
            }
        } else if (field == 'picture1' || field == 'picture2' || field == 'picture3' || field == 'picture4' || field == 'picture5' || field == 'picture6') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: "" + field + ' should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                if (field == 'picture1') {
                    picture1 = file;
                } else if (field == 'picture2') {
                    picture2 = file;
                } else if (field == 'picture3') {
                    picture3 = file;
                } else if (field == 'picture4') {
                    picture4 = file;
                } else if (field == 'picture5') {
                    picture5 = file;
                } else if (field == 'picture6') {
                    picture6 = file;
                }
            }
        }
    })
    form.on('end', async function () {
        songDetails.user_id = userId;
        if (!artistPicture) {
            res.status(422).json({ status: false, message: 'Upload an artist picture.' });
            return;
        }
        if (!audioFile) {
            res.status(422).json({ status: false, message: 'Upload an audio file.' });
            return;
        }
        if (!songCoverPicture) {
            res.status(422).json({ status: false, message: 'Upload an song cover picture.' });
            return;
        }
        if (JSON.parse(songDetails.is_sale_active) != true) {
            songDetails.sell_sale_percentage = undefined;
            songDetails.sale_price_per_percent = undefined;
            songDetails.sale_exp_date = undefined
            songDetails.sale_description = undefined;
            picture1 = undefined;
            picture2 = undefined;
            picture3 = undefined;
            picture4 = undefined;
            picture5 = undefined;
            picture6 = undefined;
        } else {
            if (!songDetails.sell_sale_percentage) {
                res.status(422).json({ status: false, message: 'Enter sale percentage.' });
                return;
            }
            if (!songDetails.sale_price_per_percent) {
                res.status(422).json({ status: false, message: 'Enter sale price.' });
                return;
            }
            if (!songDetails.sale_exp_date) {
                res.status(422).json({ status: false, message: 'Enter sale expiry date.' });
                return;
            }
            if (!songDetails.sale_description) {
                res.status(422).json({ status: false, message: 'Enter sale description.' });
                return;
            }
            if (!picture1) {
                res.status(422).json({ status: false, message: 'Upload a picture1.' });
                return;
            }
            if (!picture2) {
                res.status(422).json({ status: false, message: 'Upload a picture2.' });
                return;
            }
            if (!picture3) {
                res.status(422).json({ status: false, message: 'Upload a picture3.' });
                return;
            }
        }
        releaseSongService.releaseSong(songDetails).then(async (result) => {
            var artistPic = uploadFileToS3(artistPicture, "artistPicture")
            var audio = uploadFileToS3(audioFile, "audioFile")
            var songCoverPic = uploadFileToS3(songCoverPicture, "songCoverPicture")
            var pic1, pic2, pic3, pic4, pic5, pic6;
            if (JSON.parse(songDetails.is_sale_active) == true) {
                pic1 = uploadFileToS3(picture1, "pictures");
                pic2 = uploadFileToS3(picture2, "pictures");
                pic3 = uploadFileToS3(picture3, "pictures");
                if (picture4) pic4 = uploadFileToS3(picture4, "pictures");
                if (picture5) pic5 = uploadFileToS3(picture5, "pictures");
                if (picture6) pic6 = uploadFileToS3(picture6, "pictures");
            }
            await Promise.all([artistPic, audio, songCoverPic, pic1, pic2, pic3, pic4, pic5, pic6])
                .then(async (values) => {
                    let artistPictureURL = values[0]
                    let audioFileURL = values[1]
                    let songCoverPictureURL = values[2]
                    let pic1URL = values[3]
                    let pic2URL = values[4]
                    let pic3URL = values[5]
                    let pic4URL = values[6]
                    let pic5URL = values[7]
                    let pic6URL = values[8]
                    let AddreleaseSongMedia = releaseSongService.AddreleaseSongMedia({
                        "artist_picture": artistPictureURL, "audio_file": audioFileURL,
                        "song_cover_picture": songCoverPictureURL,
                        "picture1": pic1URL,
                        "picture2": pic2URL,
                        "picture3": pic3URL,
                        "picture4": pic4URL,
                        "picture5": pic5URL,
                        "picture6": pic6URL
                    }, result.song_release_id)
                    await Promise.all([AddreleaseSongMedia])
                        .then(async (values) => {
                            res.status(200).send({ status: true, message: "Song release successfully." })
                        }).catch((error) => {
                            logger.error('/user/song/release_song -> Post API -> Internal server error.', { obj: error });
                            res.status(500).send({ status: false, message: error })
                        })
                }).catch(async (error) => {
                    logger.error('/user/song/release_song -> Post API -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        }).catch((error) => {
            logger.error('/user/song/release_song -> Post API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
    })
    form.parse(req);
}

function uploadFileToS3(file, type) {
    return new Promise(async (resolve, reject) => {
        var Bucket;
        if (type == "artistPicture") {
            Bucket = process.env.AWS_BUCKET + "/artist_pictures";
        } else if (type == "audioFile") {
            Bucket = process.env.AWS_BUCKET + "/audio_files";
        }
        else if (type == "songCoverPicture") {
            Bucket = process.env.AWS_BUCKET + "/song_cover_pictures";
        }
        else if (type == "pictures") {
            Bucket = process.env.AWS_BUCKET + "/pictures";
        } else {
            Bucket = process.env.AWS_BUCKET
        }
        var extension = path.extname(file.name);
        var fileName = path.basename(file.name, extension);
        fileName = fileName + '-' + Date.now() + extension;
        // var fileContent = await readFileAsSync(file.path);
        const fileContent = fs.readFileSync(file.path);
        // Setting up S3 upload parameters
        const params = {
            'Key': fileName, // File name you want to save as in S3
            'Body': fileContent,
            'Bucket': Bucket,
            'ACL': "public-read",
            "ContentType": file.type
        };
        // Uploading files to the bucket
        s3.upload(params, function (err, data) {
            if (err) {
                reject(err)
            }
            resolve(data.Key);
        });

    })
}


exports.getAllReleasedSongs = async (req, res, next) => {
    releaseSongService.getAllReleasedSongs(req.query)
        .then(async (result) => {
            res.status(200).send({ "songs": result, "status": true, message: "Released songs fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/get_all_released_songs -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getSongDetails = async (req, res, next) => {
    releaseSongService.getSongDetails(req.params)
        .then(async (result) => {
            res.status(200).send({ "songs": result, "status": true, message: "Song details fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/get_all_released_songs -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.userReleases = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/user_releases -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.userReleases(userId)
        .then(async (result) => {
            res.status(200).send({ "songs": result, "status": true, message: "Songs fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/user_releases -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.userShares = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/user_shares -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.userShares(userId)
        .then(async (result) => {
            res.status(200).send({ "shares": result, "status": true, message: "shares fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/user_shares -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.detailedUserShares = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/detailed_user_shares -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.detailedUserShares(userId)
        .then(async (result) => {
            res.status(200).send({ "myShares": result, "status": true, message: "shares fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/detailed_user_shares -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.songInvestors = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/song_investors -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.songInvestors(userId, req.params)
        .then(async (result) => {
            res.status(200).send({ "songDetails": result, "status": true, message: "Investors fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/song_investors -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.userLatestInvestors = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/user_latest_investors -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.userLatestInvestors(userId)
        .then(async (result) => {
            res.status(200).send({ "latestInvestors": result, "status": true, message: "Investors fetched successfully." })
        }).catch((error) => {
            logger.error('/user/song/user_latest_investors -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.editSale = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/edit_sale -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    var songDetails = {};
    var songReleaseId;
    var picture1, picture2, picture3, picture4, picture5, picture6;
    form.on('field', function (field, value) {
        if (field == 'isSaleActive') {
            songDetails.is_sale_active = value;
        }
        else if (field == 'songReleaseId') {
            songReleaseId = value;
        }
        else if (field == 'sellSalePercentage') {
            songDetails.sell_sale_percentage = value;
        }
        else if (field == 'salePricePerPercent') {
            songDetails.sale_price_per_percent = value;
        }
        else if (field == 'saleExpiryDate') {
            songDetails.sale_exp_date = value;
        }
        else if (field == 'saleDescription') {
            songDetails.sale_description = value;
        }
    })

    form.on('file', async function (field, file) {

        if (field == 'picture1' || field == 'picture2' || field == 'picture3' || field == 'picture4' || field == 'picture5' || field == 'picture6') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: "" + field + ' should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                if (field == 'picture1') {
                    picture1 = file;
                } else if (field == 'picture2') {
                    picture2 = file;
                } else if (field == 'picture3') {
                    picture3 = file;
                } else if (field == 'picture4') {
                    picture4 = file;
                } else if (field == 'picture5') {
                    picture5 = file;
                } else if (field == 'picture6') {
                    picture6 = file;
                }
            }
        }

    })

    form.on('end', async function () {

        if (!songDetails.sell_sale_percentage) {
            res.status(422).json({ status: false, message: 'Enter sale percentage.' });
            return;
        }
        if (!songDetails.sale_price_per_percent) {
            res.status(422).json({ status: false, message: 'Enter sale price.' });
            return;
        }
        if (!songDetails.sale_exp_date) {
            res.status(422).json({ status: false, message: 'Enter sale expiry date.' });
            return;
        }
        if (!songDetails.sale_description) {
            res.status(422).json({ status: false, message: 'Enter sale description.' });
            return;
        }
        if (!songReleaseId) {
            res.status(422).json({ status: false, message: 'Enter Song release Id.' });
            return;
        }

        var pic1, pic2, pic3, pic4, pic5, pic6;
        if (picture1) pic1 = uploadFileToS3(picture1, "pictures");
        if (picture2) pic2 = uploadFileToS3(picture2, "pictures");
        if (picture3) pic3 = uploadFileToS3(picture3, "pictures");
        if (picture4) pic4 = uploadFileToS3(picture4, "pictures");
        if (picture5) pic5 = uploadFileToS3(picture5, "pictures");
        if (picture6) pic6 = uploadFileToS3(picture6, "pictures");

        await Promise.all([pic1, pic2, pic3, pic4, pic5, pic6])
            .then(async (values) => {
                if (values[0]) songDetails.picture1 = values[0];
                if (values[1]) songDetails.picture2 = values[1];
                if (values[2]) songDetails.picture3 = values[2];
                if (values[3]) songDetails.picture4 = values[3];
                if (values[4]) songDetails.picture5 = values[4];
                if (values[5]) songDetails.picture6 = values[5];

                releaseSongService.editSale(userId, songReleaseId, songDetails)
                    .then(async (result) => {
                        if (result == 1) {
                            res.status(200).send({ "status": true, message: "Sale edited successfully." })
                        } else {
                            res.status(400).send({ "status": false, message: "Sale is not edited." })
                        }
                    }).catch((error) => {
                        logger.error('/user/song/edit_sale -> Post API -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            }).catch(async (error) => {
                logger.error('/user/song/edit_sale -> Post API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })

    })
    form.parse(req);
}

exports.newSale = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/new_sale -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    var songDetails = {};
    var songReleaseId;
    var picture1, picture2, picture3, picture4, picture5, picture6;
    form.on('field', function (field, value) {
        if (field == 'isSaleActive') {
            songDetails.is_sale_active = value;
        }
        else if (field == 'songReleaseId') {
            songReleaseId = value;
        }
        else if (field == 'sellSalePercentage') {
            songDetails.sell_sale_percentage = value;
        }
        else if (field == 'salePricePerPercent') {
            songDetails.sale_price_per_percent = value;
        }
        else if (field == 'saleExpiryDate') {
            songDetails.sale_exp_date = value;
        }
        else if (field == 'saleDescription') {
            songDetails.sale_description = value;
        }
    })

    form.on('file', async function (field, file) {

        if (field == 'picture1' || field == 'picture2' || field == 'picture3' || field == 'picture4' || field == 'picture5' || field == 'picture6') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: "" + field + ' should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                if (field == 'picture1') {
                    picture1 = file;
                } else if (field == 'picture2') {
                    picture2 = file;
                } else if (field == 'picture3') {
                    picture3 = file;
                } else if (field == 'picture4') {
                    picture4 = file;
                } else if (field == 'picture5') {
                    picture5 = file;
                } else if (field == 'picture6') {
                    picture6 = file;
                }
            }
        }

    })

    form.on('end', async function () {

        if (!songDetails.sell_sale_percentage) {
            res.status(422).json({ status: false, message: 'Enter sale percentage.' });
            return;
        }
        if (!songDetails.sale_price_per_percent) {
            res.status(422).json({ status: false, message: 'Enter sale price.' });
            return;
        }
        if (!songDetails.sale_exp_date) {
            res.status(422).json({ status: false, message: 'Enter sale expiry date.' });
            return;
        }
        if (!songDetails.sale_description) {
            res.status(422).json({ status: false, message: 'Enter sale description.' });
            return;
        }
        if (!songReleaseId) {
            res.status(422).json({ status: false, message: 'Enter Song release Id.' });
            return;
        }

        var pic1, pic2, pic3, pic4, pic5, pic6;
        if (picture1) pic1 = uploadFileToS3(picture1, "pictures");
        if (picture2) pic2 = uploadFileToS3(picture2, "pictures");
        if (picture3) pic3 = uploadFileToS3(picture3, "pictures");
        if (picture4) pic4 = uploadFileToS3(picture4, "pictures");
        if (picture5) pic5 = uploadFileToS3(picture5, "pictures");
        if (picture6) pic6 = uploadFileToS3(picture6, "pictures");


        await Promise.all([pic1, pic2, pic3, pic4, pic5, pic6])
            .then(async (values) => {
                if (values[0]) songDetails.picture1 = values[0];
                if (values[1]) songDetails.picture2 = values[1];
                if (values[2]) songDetails.picture3 = values[2];
                if (values[3]) songDetails.picture4 = values[3];
                if (values[4]) songDetails.picture5 = values[4];
                if (values[5]) songDetails.picture6 = values[5];

                releaseSongService.editSale(userId, songReleaseId, songDetails)
                    .then(async (result) => {
                        if (result == 1) {
                            res.status(200).send({ "status": true, message: "New Sale added successfully." })
                        } else {
                            res.status(400).send({ "status": false, message: "New sale is not added." })
                        }
                    }).catch((error) => {
                        logger.error('/user/song/new_sale -> Put API -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            }).catch(async (error) => {
                logger.error('/user/song/new_sale -> Put API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })

    })
    form.parse(req);
}

exports.cancelRelease = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/cancel_release -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    releaseSongService.cancelRelease(userId, req.params.songReleaseId)
        .then(async (result) => {
            if (result == 1) {
                res.status(200).send({ "status": true, message: "Release cancelled successfully." })
            } else {
                res.status(400).send({ "status": false, message: "Release is not cancelled." })
            }
        }).catch((error) => {
            logger.error('/user/song/cancel_release -> Delete API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.closeSale = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/song/close_sale -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let body = {
        is_sale_active: false,
    }

    releaseSongService.editSale(userId, req.body.songReleaseId, body)
        .then(async (result) => {
            if (result == 1) {
                res.status(200).send({ "status": true, message: "Sale closed successfully." })
            } else {
                res.status(400).send({ "status": false, message: "Sale is not closed." })
            }
        }).catch((error) => {
            logger.error('/user/song/close_sale -> Put API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};