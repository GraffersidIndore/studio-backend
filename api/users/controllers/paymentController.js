const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
var jwt = require('../../lib/jwt');

var userDetailsService = require("../../services/userDetailsService")
var releaseSongService = require("../../services/releaseSongService")

const Sequelize = require('../../../config/database');
const { SongReleases, UserShares, MoneyTransactions, Users } = require('../../models');
const { Op, fn, col } = require('sequelize');

const stripe = require('stripe')(process.env.NODE_ENV == "development" ? process.env.STRIPE_TEST_SECRET_KEY : process.env.STRIPE_LIVE_SECRET_KEY);


exports.buyShare = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/payment/buy_share -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { songReleaseId, cardId, purchasedSalePercentage } = req.body

    let cardDetails = userDetailsService.cardDetails(userId, cardId)
    let userDetails = userDetailsService.userDetails(userId)
    let songReleaseDetails = releaseSongService.getSongDetails({ "songReleaseId": songReleaseId })

    await Promise.all([cardDetails, songReleaseDetails, userDetails])
        .then(async (values) => {

            const { card_number, exp_date, cvc, country, zip_code } = values[0];
            if (!card_number || !exp_date || !cvc || !country || !zip_code) {
                return res.status(400).send({ status: false, message: "Necessary Card Details are required for Payment", });
            }
            let d = new Date(exp_date);
            let cardExpMonth = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
            let cardExpYear = d.getFullYear();

            const { song_release_id, sale_price_per_percent, sell_sale_percentage, sold_sale_percentage, sale_exp_date, sale_description } = values[1];
            if (!values[1].is_sale_active) {
                return res.status(400).send({ status: false, message: "No active sale, can't buy the share", });
            }
            if (sell_sale_percentage - sold_sale_percentage < parseInt(purchasedSalePercentage)) {
                return res.status(400).send({ status: false, message: `You can't buy ${purchasedSalePercentage}% share. Remaining share ${sell_sale_percentage - sold_sale_percentage}%`, });
            }

            const { email } = values[2];

            const totalSharePurchasedPrice = sale_price_per_percent * parseInt(purchasedSalePercentage);

            let buyShareBody = {
                user_id: userId,
                song_release_id: song_release_id,
                sale_price_per_percent: sale_price_per_percent,
                sale_exp_date: sale_exp_date,
                sale_description: sale_description,
                purchased_percentage_sale: parseInt(purchasedSalePercentage),
                total_purchased_price: totalSharePurchasedPrice,
            }
            const t = await Sequelize.transaction();

            let userShare = await UserShares.create(buyShareBody, { transaction: t });
            let totalSoldSalePercentage = sold_sale_percentage + parseInt(purchasedSalePercentage)
            SongReleases.update({ "sold_sale_percentage": totalSoldSalePercentage }, { where: { "song_release_id": songReleaseId }, transaction: t });
            try {
                const cardToken = await stripe.tokens.create({
                    card: {
                        number: card_number,
                        exp_month: cardExpMonth,
                        exp_year: cardExpYear,
                        cvc: cvc,
                        address_state: country,
                        address_zip: zip_code,
                    },
                });
                const amountInPence = totalSharePurchasedPrice * 100;
                const charge = await stripe.charges.create({
                    amount: amountInPence,
                    currency: "usd",
                    source: cardToken.id,
                    receipt_email: email,
                    description: `Buys share of ${totalSharePurchasedPrice}`,
                });

                if (charge.status === "succeeded") {
                    let transactionBody = {
                        user_id: userId,
                        song_release_id: songReleaseId,
                        transaction_id: charge.id,
                        transaction_type: "share purches",
                        transaction_amount: totalSharePurchasedPrice,
                        commission_amount: 0,
                        transaction_status: true,
                    }
                    await t.commit();
                    await MoneyTransactions.create(transactionBody);
                    return res.status(200).send({ status: true, message: "bought share successfully" });
                } else {
                    let transactionBody = {
                        user_id: userId,
                        song_release_id: songReleaseId,
                        transaction_id: charge.id,
                        transaction_type: "share purches",
                        transaction_amount: totalSharePurchasedPrice,
                        commission_amount: 0,
                        transaction_status: false,
                    }
                    await t.rollback();
                    await MoneyTransactions.create(transactionBody);
                    return res.status(400).send({ status: true, message: "Please try again later for Payment" });
                }
            } catch (error) {
                await t.rollback();
                logger.error('/user/payment/buy_share -> Post API -> Internal server error.', { obj: error });
                return res.status(500).send({ status: false, message: error.raw.message, });
            }
        }).catch(async (error) => {
            logger.error('/user/payment/buy_share -> Post API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.withdrawMoney = async (req, res, next) => {

    const cardToken = await stripe.tokens.create({
        card: {
            number: "4111 1111 1111 1111",
            exp_month: "2",
            exp_year: "2024",
            cvc: "123",
            address_state: "india",
            address_zip: "452001",
        },
    });

    console.log(cardToken.id)
    const payout = await stripe.payouts.create({
        amount: 1000,
        currency: 'usd',
        method: 'instant',
        source_type: "card",
        destination: cardToken.id
    });

    console.log(payout)
}