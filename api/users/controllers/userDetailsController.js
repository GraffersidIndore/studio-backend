const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
var bcrypt = require('../../lib/bcrypt');
var jwt = require('../../lib/jwt');
var moment = require('moment');

var path = require('path');
const fs = require('fs');

var formidable = require('formidable');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    region: "us-east-2",
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const userDetailsService = require('../../services/userDetailsService')
const userAuthService = require('../../services/userAuthService')

exports.userDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/user_details -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.userDetails(userId)
        .then(async (result) => {
            res.status(200).send({ userDetails: result, "status": true, message: "User detail fetched successfully." })
        }).catch((error) => {
            logger.error('/user/details/user_details -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.editUserDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/user_details -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { firstName, lastName, email, country, dateOfBirth } = req.body
    var body = {
        firstname: firstName,
        lastname: lastName,
        email: email,
        country: country,
        date_of_birth: dateOfBirth
    }
    if (req.body.firstName) {
        body.firstname = req.body.firstName;
    }
    if (req.body.lastName) {
        body.lastname = req.body.lastName;
    }
    if (req.body.email) {
        body.email = req.body.email.toLowerCase();
        let uniqueEmail = await userAuthService.isEmailUnique(email)
        if (!uniqueEmail) {
            res.status(400).send({ status: false, message: "Email is already in use." });
            return
        }
    }
    if (req.body.dateOfBirth) {
        body.date_of_birth = req.body.dateOfBirth;
    }
    if (req.body.country) {
        body.country = req.body.country;
    }

    userDetailsService.editUserDetails(userId, body)
        .then(async (result) => {
            res.status(200).send({ "status": true, message: "Detail edited successfully." })
        }).catch((error) => {
            logger.error('/user/details/user_details -> Patch API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error.obj.error })
        })

}

exports.editProfilePicture = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/edit_profile_picture -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    var profilePicture;
    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });

    form.on('file', async function (field, file) {
        if (field == 'profilePicture') {
            if (file.name) {
                let fileType = file.type
                fileType = fileType.split("/", 1)[0]
                if (fileType) {
                    if (!(fileType == "image")) {
                        res.status(422).json({ status: false, message: 'Profile picture should be image.' });
                        return;
                    }
                } else {
                    res.status(422).json({ status: false, message: 'Invalid file' });
                    return;
                }
                profilePicture = file;
            }
        }
    })
    form.on('end', async function () {
        if (!profilePicture) {
            res.status(422).json({ status: false, message: 'Upload a profile picture.' });
            return;
        }
        let profilePic = await uploadFileToS3(profilePicture, "profilePicture");
        let body = {
            profile_picture: profilePic
        }
        userDetailsService.editUserDetails(userId, body)
            .then(async (result) => {
                res.status(200).send({ "status": true, message: "profile picture edited successfully." })
            }).catch((error) => {
                logger.error('/user/details/edit_profile_picture -> Patch API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    })
    form.parse(req);
}

exports.changePassword = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/change_password -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { password, oldPassword } = req.body;

    await userDetailsService.getOldPassword(userId)
        .then(async (user) => {
            var passwordIsValid = bcrypt.comparePassword(user.password, oldPassword);//hashPassword,password
            if (!passwordIsValid) {
                res.status(403).send({ status: false, message: "Entered wrong old password." });
            } else {
                let hashedpassword = await bcrypt.hashPassword(password)
                userAuthService.resetPassword(hashedpassword, userId)
                    .then(async (result) => {
                        if (result == 1) {
                            res.status(200).send({ "status": true, message: "Password changed successfully." })
                        } else {
                            res.status(200).send({ "status": false, message: "Password is not changed." })
                        }

                    }).catch((error) => {
                        logger.error('/user/details/change_password -> Patch API -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            }
        }).catch((error) => {
            logger.error('/user/details/change_password -> Patch API -> Internal server error.');
            res.status(500).send({ status: false, message: error })
        })

}

exports.cardLists = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/card_lists -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.cardLists(userId)
        .then(async (result) => {
            res.status(200).send({ cardDetails: result, "status": true, message: "Card lists fetched successfully." })
        }).catch((error) => {
            logger.error('/user/details/card_lists -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.addCardDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/card_details -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { firstName, cardNumber, expiryDate, cvc, zipCode, country } = req.body
    var d = new Date();
    var date = d.getDate();
    const format2 = "YYYY-MM-DD"
    var splitDate = expiryDate.split("/");
    var month = splitDate[0];
    var year = splitDate[1];
    let expirationDate = moment(month + "/" + date + '/' + year).format(format2);
    let body = {
        "user_id": userId,
        "first_name": firstName,
        "card_number": cardNumber,
        "exp_date": expirationDate,
        "cvc": cvc,
        "zip_code": zipCode,
        "country": country
    }

    userDetailsService.addCardDetails(body)
        .then(async (result) => {
            res.status(200).send({ cardDetails: result, "status": true, message: "Card details added successfully." })
        }).catch((error) => {
            logger.error('/user/details/card_details -> Post API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.editCardDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/card_details -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { cardId, firstName, cardNumber, expiryDate, cvc, zipCode, country } = req.body
    var d = new Date();
    var date = d.getDate();
    const format2 = "YYYY-MM-DD"
    var splitDate = expiryDate.split("/");
    var month = splitDate[0];
    var year = splitDate[1];
    let expirationDate = moment(month + "/" + date + '/' + year).format(format2);
    let body = {
        "first_name": firstName,
        "card_number": cardNumber,
        "exp_date": expirationDate,
        "cvc": cvc,
        "zip_code": zipCode,
        "country": country
    }

    userDetailsService.editCardDetails(userId, cardId, body)
        .then(async (result) => {
            res.status(200).send({ "status": true, message: "Card details edited successfully." })
        }).catch((error) => {
            logger.error('/user/details/card_details -> Put API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.deleteAccount = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/delete_account -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.deleteAccount(userId)
        .then(async (result) => {
            res.status(200).send({ "status": true, message: "Account deleted successfully." })
        }).catch((error) => {
            logger.error('/user/details/delete_account -> Delete API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

function uploadFileToS3(file, type) {
    return new Promise(async (resolve, reject) => {
        var Bucket;
        if (type == "profilePicture") {
            Bucket = process.env.AWS_BUCKET + "/profile_pictures";
        } else {
            Bucket = process.env.AWS_BUCKET
        }
        var extension = path.extname(file.name);
        var fileName = path.basename(file.name, extension);
        fileName = fileName + '-' + Date.now() + extension;
        // var fileContent = await readFileAsSync(file.path);
        const fileContent = fs.readFileSync(file.path);
        // Setting up S3 upload parameters
        const params = {
            'Key': fileName, // File name you want to save as in S3
            'Body': fileContent,
            'Bucket': Bucket,
            'ACL': "public-read",
            "ContentType": file.type
        };
        // Uploading files to the bucket
        s3.upload(params, function (err, data) {
            if (err) {
                reject(err)
            }
            resolve(data.Location);
        });

    })
}