const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
var jwt = require('../../lib/jwt');

const notificationsService = require("../../services/notificationsService")

exports.getUserNotifications = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/notification/get_user_notifications -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    notificationsService.getUserNotifications(userId)
        .then(async (result) => {
            res.status(200).send({ notifications: result, "status": true, message: "Notifications fetched successfully." })
        }).catch((error) => {
            logger.error('/user/notification/get_user_notifications -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.deleteNotification = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/notification/delete_notification/:notificationId -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    notificationsService.deleteNotification(userId, req.params)
        .then(async (result) => {
            res.status(200).send({ "status": true, message: "Notifications deleted successfully." })
        }).catch((error) => {
            logger.error('/user/notification/delete_notification/:notificationId -> Delete API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}