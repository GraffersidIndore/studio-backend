const { validationResult } = require('express-validator');
const crypto = require('crypto');
const { logger } = require("../../../config/logger");
var bcrypt = require('../../lib/bcrypt');
var jwt = require('../../lib/jwt');

const mailer = require("@sendgrid/mail");
mailer.setApiKey(`${process.env.SENDGRID_API_KEY}`);
let ejs = require('ejs');
var fs = require('fs');
var path = require('path');
var verifyAccountTemplate = fs.readFileSync(`${path.join(__dirname, "../../../", "views/email_template/verifyAccount.ejs")}`, { encoding: 'utf8', flag: 'r' });
var compiledVerifyAccountTemplate = ejs.compile(verifyAccountTemplate)

var resetPasswordTemplate = fs.readFileSync(`${path.join(__dirname, "../../../", "views/email_template/resetPassword.ejs")}`, { encoding: 'utf8', flag: 'r' });
var compiledResetPasswordTemplate = ejs.compile(resetPasswordTemplate)

//require service
const userAuthService = require('../../services/userAuthService');

exports.registerUser = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { email, password } = req.body;
    let isEmailUnique = await userAuthService.isEmailUnique(email);

    if (isEmailUnique) {
        let hashedpassword = await bcrypt.hashPassword(password)
        userAuthService.registerUser(req.body, hashedpassword)
            .then(async result => {
                if (result !== null) {

                    jwt.generateToken(result.user_id, result.user_role).then((token) => {
                        userAuthService.setUserAccessToken(token, result.user_id)
                            .then(result2 => {
                                let fullName = result.firstname + " " + result.lastname
                                verifyAccountMail(result.email, fullName, token).then((result3) => {
                                    res.status(200).send({ "user_id": result.user_id, "email": result.email, status: true, message: "User registered successfully" });
                                }).catch((error) => {
                                    logger.error('/user/auth/register -> Internal server error.', { obj: error });
                                    res.status(500).send({ status: false, message: error })
                                })
                            }).catch((error) => {
                                logger.error('/user/auth/register -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: error })
                            })
                    });

                }
            })
    } else {
        res.status(400).send({ status: false, message: "Email is already in use." });
    }
}

exports.login = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { email, password } = req.body;

    userAuthService.login(email)
        .then(async user => {
            if (user !== null) {

                const { is_active, is_verified, user_id, user_role } = user;
                if (user_role !== "user") {
                    res.status(404).send({ status: false, message: "Account is not valid." });
                } else if (!is_verified) {
                    await jwt.generateToken(user_id, user_role).then(async (token) => {
                        await userAuthService.setUserAccessToken(token, user_id)
                            .then(async result => {
                                let fullName = result.firstname + " " + result.lastname
                                await verifyAccountMail(result.email, fullName, token).then((result3) => {
                                    res.status(403).send({ "user_id": user_id, "email": user.email, status: false, error: "AccountNotVerified", message: "Account is not verified" });
                                    return;
                                }).catch((error) => {
                                    logger.error('/user/auth/login -> Internal server error.', { obj: error });
                                    res.status(500).send({ status: false, message: error })
                                })
                            }).catch((error) => {
                                logger.error('/user/auth/login -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: error })
                            })
                    });

                } else if (!is_active) {
                    res.status(403).send({ status: false, error: "accountDeleted", message: "This account is deleted." });
                    return;
                } else {

                    var passwordIsValid = bcrypt.comparePassword(user.password, password);//hashPassword,password
                    if (passwordIsValid) {
                        jwt.generateToken(user_id, user_role).then((token) => {
                            userAuthService.setUserAccessToken(token, user_id)
                                .then(result => {
                                    res.status(200).send({ "email": result.email, "firstname": result.firstname, "lastname": result.lastname, "profile_picture": result.profile_picture, "access_token": result.access_token, status: true, message: "Loggedin successfully" });
                                }).catch((error) => {
                                    logger.error('/user/auth/login -> Internal server error.', { obj: error });
                                    res.status(500).send({ status: false, message: error })
                                })
                        });
                    } else {
                        res.status(403).send({ status: false, message: "Entered wrong password" });
                    }
                }
            } else {
                res.status(404).send({ status: false, message: "User doesn't exist" });
            }

        }).catch((error) => {
            logger.error('/user/auth/login -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.socialSignIn = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userAuthService.socialSignIn(req.body)
        .then(async user => {
            jwt.generateToken(user.user_id, user.user_role).then((token) => {
                userAuthService.setUserAccessToken(token, user.user_id)
                    .then(result => {
                        res.status(200).send({ "email": result.email, "firstname": result.firstname, "lastname": result.lastname, "profile_picture": result.profile_picture, "access_token": result.access_token, status: true, message: "Loggedin successfully" });
                    }).catch((error) => {
                        logger.error('/user/auth/social_sign_in -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            });
        }).catch((error) => {
            logger.error('/user/auth/social_sign_in -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Login User
exports.logout = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/auth/logout -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userAuthService.logout(userId)
        .then(user => {
            if (user !== null) {
                res.status(200).send({ status: true, message: "Logout Successfully" });
            } else {
                res.status(401).send({ status: false, message: "Something went wrong." });
            }
        }).catch((error) => {
            logger.error('/user/auth/logout -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.sendAccountVerificationLink = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { userId } = req.body
    jwt.generateToken(userId).then((token) => {
        userAuthService.setUserAccessToken(token, userId)
            .then(result => {
                let fullName = result.firstname + " " + result.lastname
                verifyAccountMail(result.email, fullName, token).then((result3) => {
                    res.status(200).send({ status: true, message: "You will receive account verification link on your registered email account." });
                }).catch((error) => {
                    logger.error('/user/auth/send_account_verification_link -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
            }).catch((error) => {
                logger.error('/user/auth/send_account_verification_link -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
}

exports.verifyAccount = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { verificationToken } = req.body

    userAuthService.verifyAccount(verificationToken)
        .then(result => {
            if (result !== null) {
                res.status(200).send({ status: true, message: "Account verified successfully" });
            } else {
                res.status(400).send({ status: false, error: "invalidLink", message: "Verification link is invalid." });
            }
        }).catch((error) => {
            logger.error('/user/auth/verify_account -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

// -> Forgot Password
exports.forgotPassword = async (req, res, next) => {
    // -> Request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    const { email } = req.body
    userAuthService.isUserExist(email)
        .then(async user => {
            if (user !== null) {
                const { user_id } = user;
                const buf = crypto.randomBytes(20);
                var token = buf.toString('hex');
                let tokenExpiryTime = await userAuthService.setTokenExpiryTime();
                userAuthService.setResetPasswordTokenAndTime(token, tokenExpiryTime, user_id)
                    .then(async result => {
                        if (result !== null) {
                            forgotPasswordMail(result.email, result.reset_password_token, `${result.firstname} ${result.lastname}`).then((result2) => {
                                res.status(200).send({ status: true, message: "You will receive reset password link on your registered email account" });
                            }).catch((error) => {
                                logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: "Something went wrong, Internal server error." })
                            })
                        }
                    }).catch((error) => {
                        logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })

            } else {
                res.status(404).send({ status: false, message: "Email doesn't exist." });
            }

        }).catch((error) => {
            logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Reset Password
exports.resetPassword = async (req, res, next) => {
    //  -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).send({ status: false, errors: errors.array() });
        return;
    }

    let date = await userAuthService.getDate();
    const { token, password } = req.body;
    userAuthService.checkTokenExpiryTime(token, date)
        .then(async user => {
            if (user !== null) {
                const { user_id } = user
                let hashedpassword = await bcrypt.hashPassword(password)
                userAuthService.resetPassword(hashedpassword, user_id)
                    .then(result => {
                        res.status(200).send({ status: true, message: "Password reset successfully." });
                    }).catch((error) => {
                        logger.error('/user/auth/reset_password -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            } else {
                res.status(400).send({ status: false, message: 'Reset password link is invalid or has expired.' });
            }

        }).catch((error) => {
            logger.error('/user/auth/reset_password -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

verifyAccountMail = async (email, name, token) => {
    return new Promise(async (resolve, reject) => {
        try {
            // Setting configurations 
            const msg = {
                to: `${email}`,
                from: {
                    "email": `${process.env.MAILER_EMAIL_ID}`,
                    "name": "StudioPop"
                },
                subject: "Verify your Account",
                html: compiledVerifyAccountTemplate({ 'name': `${name}`, Link: (process.env.NODE_ENV == "development" ? `${'http://localhost:4200/verify_account/'}${token}` : `${'http://3.129.32.62/verify_account/'}${token}`) })
            };

            // Sending mail 
            mailer.send(msg, function (err, json) {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            });

        } catch (error) {
            reject(error)
        }
    })
}

forgotPasswordMail = async (email, token, name) => {
    return new Promise(async (resolve, reject) => {
        try {
            // Setting configurations 
            const msg = {
                to: `${email}`,
                from: {
                    "email": `${process.env.MAILER_EMAIL_ID}`,
                    "name": "StudioPop"
                },
                subject: "Reset Password Link",
                html: compiledResetPasswordTemplate({ firstname: `${name}`, Link: (process.env.NODE_ENV == "development" ? `http://localhost:4200/reset_password/${token}` : `http://3.129.32.62/reset_password/${token}`) })
            };

            // Sending mail 
            mailer.send(msg, function (err, json) {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            });

        } catch (error) {
            reject(error)
        }
    })
}