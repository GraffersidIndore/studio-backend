var express = require('express');
var router = express.Router();

var userAuth = require('./routes/userAuth');
var releaseSong = require('./routes/releaseSong');
var userDetails = require('./routes/userDetails');
var payment = require('./routes/payment');
var notification = require("./routes/notifications")

// ->  User Authentication
router.use('/auth', userAuth);

// ->  User Authentication
router.use('/song', releaseSong);

// ->  User Details
router.use('/details', userDetails);

// ->  User Details
router.use('/payment', payment);

// ->  User Details
router.use('/notification', notification);

module.exports = router;