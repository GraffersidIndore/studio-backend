var express = require('express');
var router = express.Router();

const userAuthvalidator = require('../../middleware/validators/userAuthValidator')

const userAuthController = require('../controllers/userAuthController')

// -> Register User to Database
router.post('/register', userAuthvalidator.validate("registerUser"),
    userAuthController.registerUser
);

// -> verify User Account
router.post('/login', userAuthvalidator.validate("login"),
    userAuthController.login
);

// -> Social signIn
router.post('/social_sign_in', userAuthvalidator.validate("social_sign_in"),
    userAuthController.socialSignIn
);

// -> logout User
router.get('/logout', userAuthvalidator.validate("headers"),
    userAuthController.logout
);

// send account verification link
router.post('/send_account_verification_link', userAuthvalidator.validate("sendAccountVerificationLink"),
    userAuthController.sendAccountVerificationLink
);

//Verify Accounf
router.post('/verify_account', userAuthvalidator.validate("verifyAccount"),
    userAuthController.verifyAccount
);

// -> Forgot Password
router.post('/forgot_password', userAuthvalidator.validate("forgotPassword"),
    userAuthController.forgotPassword
);

// -> Reset Password
router.post('/reset_password', userAuthvalidator.validate("resetPassword"),
    userAuthController.resetPassword
);

module.exports = router;