var express = require('express');
var router = express.Router();

const userAuthValidator = require('../../middleware/validators/userAuthValidator');
const paymentValidators = require('../../middleware/validators/paymentValidators');
const auth = require('../../middleware/authentication')

const paymentController = require('../controllers/paymentController')

// -> Register User to Database
router.post('/buy_share',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    paymentValidators.validate("buyShare"),
    paymentController.buyShare
);

router.post('/withdraw_money',
    // userAuthValidator.validate("headers"),
    // auth.isUserAuthenticated,
    paymentController.withdrawMoney
);


module.exports = router;