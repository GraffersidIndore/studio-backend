var express = require('express');
var router = express.Router();

const releaseSongValidator = require('../../middleware/validators/releaseSongValidator')
const userAuthValidator = require('../../middleware/validators/userAuthValidator')

const releaseSongController = require('../controllers/releaseSongController')

const auth = require('../../middleware/authentication')

// -> Release a song
router.post('/release_song',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    releaseSongController.releaseSong
);

// -> edit a sale
router.put('/edit_sale',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    releaseSongController.editSale
);

// -> new sale
router.put('/new_sale',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    releaseSongController.newSale
);

// -> cancel release
router.delete('/cancel_release/:songReleaseId',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    releaseSongController.cancelRelease
);

// -> Close sale
router.put('/close_sale',
    userAuthValidator.validate("headers"),
    auth.isUserAuthenticated,
    releaseSongValidator.validate("songReleaseIdCheck"),
    releaseSongController.closeSale
);

// -> Get all the released song
router.get('/get_all_released_songs',
    releaseSongController.getAllReleasedSongs
);

// -> Get song Details
router.get('/song_details/:songReleaseId',
    releaseSongController.getSongDetails
);

// -> Get user releases
router.get('/user_releases', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    releaseSongController.userReleases
);

// -> Get user shares
router.get('/user_shares', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    releaseSongController.userShares
);

// -> Get detailed user shares
router.get('/detailed_user_shares', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    releaseSongController.detailedUserShares
);

// -> Get song investors
router.get('/song_investors/:songReleaseId', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    releaseSongController.songInvestors
);

// -> Get latest releases
router.get('/user_latest_investors', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    releaseSongController.userLatestInvestors
);

module.exports = router;