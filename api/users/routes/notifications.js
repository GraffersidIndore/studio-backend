var express = require('express');
var router = express.Router();

const notificationsController = require("../controllers/notificationsController")
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const auth = require('../../middleware/authentication')

// -> Get user notification
router.get('/get_user_notifications', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    notificationsController.getUserNotifications
);

// -> Get user notification
router.delete('/delete_notification/:notificationId', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    notificationsController.deleteNotification
);

module.exports = router;