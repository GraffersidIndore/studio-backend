var express = require('express');
var router = express.Router();

const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const userDetailsValidator = require('../../middleware/validators/userDetailsValidator')
const userDetailsController = require('../controllers/userDetailsController')
const auth = require('../../middleware/authentication')


// -> Get user details
router.get('/user_details', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsController.userDetails
);

// -> edit user details
router.patch('/user_details', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsController.editUserDetails
);

// -> edit user details
router.patch('/edit_profile_picture', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsController.editProfilePicture
);

// -> Change Password
router.patch('/change_password', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsValidator.validate("changePassword"), userDetailsController.changePassword
);

// -> Get card lists
router.get('/card_lists', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsController.cardLists
);

// -> Add card details
router.post('/card_details', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsValidator.validate("addCardDetails"), userDetailsController.addCardDetails
);

// -> edit card details
router.put('/card_details', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsValidator.validate("editCardDetails"), userDetailsController.editCardDetails
);

// -> edit card details
router.delete('/delete_account', userAuthValidator.validate("headers"), auth.isUserAuthenticated,
    userDetailsController.deleteAccount
);

module.exports = router;
