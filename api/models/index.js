const Sequelize = require('sequelize')
// require('sequelize-hierarchy')(Sequelize);
const db = require('../../config/database');

// -> Import Models
const Users = require('./users')(db, Sequelize);
const CardDetails = require('./card_details')(db, Sequelize);
const SongReleases = require('./song_releases')(db, Sequelize);
const UserShares = require('./user_shares')(db, Sequelize);
const Notificatons = require('./notifications')(db, Sequelize);
const MoneyTransactions = require('./money_transactions')(db, Sequelize);

// -> Relationship Between The Tables

// -> Associtaion Relationship of users table
Users.hasMany(SongReleases, { as: "userSongReleases", foreignKey: 'user_id' });
SongReleases.belongsTo(Users, { as: "SongReleaseUserDetails", foreignKey: 'user_id' })

Users.hasMany(CardDetails, { as: "userCards", foreignKey: 'user_id' });
CardDetails.belongsTo(Users, { foreignKey: 'user_id' })


// -> Associtaion Relationship of user_shares table
Users.belongsToMany(SongReleases, { through: UserShares, foreignKey: 'user_id' });
SongReleases.belongsToMany(Users, { through: UserShares, foreignKey: 'song_release_id' });

SongReleases.hasMany(UserShares, { as: "songInvestors", foreignKey: 'song_release_id' });
UserShares.belongsTo(SongReleases, { as: "songDetails", foreignKey: 'song_release_id' });

Users.hasMany(UserShares, { as: "userShares", foreignKey: 'user_id' });
UserShares.belongsTo(Users, { as: "investorsDetail", foreignKey: 'user_id' });


// -> Associtaion Relationship of notifications table
Users.belongsToMany(SongReleases, { through: Notificatons, foreignKey: 'user_id' });
SongReleases.belongsToMany(Users, { through: Notificatons, foreignKey: 'song_release_id' });

SongReleases.hasMany(Notificatons, { as: "songReleaseNotification", foreignKey: 'song_release_id' });
Notificatons.belongsTo(SongReleases, { foreignKey: 'song_release_id' });

Users.hasMany(Notificatons, { as: "userNotification", foreignKey: 'user_id' });
Notificatons.belongsTo(Users, { foreignKey: 'user_id' });


// -> Associtaion Relationship of money_transacion table
Users.belongsToMany(SongReleases, { through: MoneyTransactions, foreignKey: 'user_id' });
SongReleases.belongsToMany(Users, { through: MoneyTransactions, foreignKey: 'song_release_id' });

SongReleases.hasMany(MoneyTransactions, { as: "songTransactionDetails", foreignKey: 'song_release_id' });
MoneyTransactions.belongsTo(SongReleases, { as: "transactionSongDetails", foreignKey: 'song_release_id' });

Users.hasMany(MoneyTransactions, { as: "userTransaction", foreignKey: 'user_id' });
MoneyTransactions.belongsTo(Users, { as: "transactionUserDetails", foreignKey: 'user_id' });

module.exports = {
    Users,
    CardDetails,
    SongReleases,
    UserShares,
    Notificatons,
    MoneyTransactions
}