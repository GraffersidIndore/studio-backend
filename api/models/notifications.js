module.exports = (db, Sequelize) => {
    const notification = db.define('notification', {
        notification_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        user_id: {
            type: Sequelize.STRING
        },
        song_release_id: {
            type: Sequelize.STRING
        },
        notification_message: {
            type: Sequelize.STRING
        },
        notification_type: {
            type: Sequelize.ENUM(['release confirmed', 'release rejected'])
        },
        is_read: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return notification;
}