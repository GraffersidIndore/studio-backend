module.exports = (db, Sequelize) => {
    const cardDetails = db.define('card_details', {
        card_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        user_id: {
            type: Sequelize.STRING
        },
        first_name: {
            type: Sequelize.STRING
        },
        card_number: {
            type: Sequelize.INTEGER
        },
        exp_date: {
            type: Sequelize.DATEONLY
        },
        cvc: {
            type: Sequelize.INTEGER
        },
        zip_code: {
            type: Sequelize.STRING
        },
        country: {
            type: Sequelize.STRING
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return cardDetails;
}