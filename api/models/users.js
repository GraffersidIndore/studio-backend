module.exports = (db, Sequelize) => {
    const users = db.define('users', {
        user_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        firstname: {
            type: Sequelize.STRING
        },
        lastname: {
            type: Sequelize.STRING
        },
        date_of_birth: {
            type: Sequelize.DATEONLY
        },
        country: {
            type: Sequelize.STRING
        },
        profile_picture: {
            type: Sequelize.STRING
        },
        outstanding_balance: {
            type: Sequelize.DECIMAL(15, 2)
        },
        all_time_income: {
            type: Sequelize.DECIMAL(15, 2)
        },
        last_password_updated_at: {
            type: Sequelize.DATE
        },
        last_loggedin_at: {
            type: Sequelize.DATE
        },
        access_token: {
            type: Sequelize.STRING
        },
        user_role: {
            type: Sequelize.ENUM(['user', 'admin'])
        },
        reset_password_token: {
            type: Sequelize.STRING
        },
        reset_password_expires: {
            type: Sequelize.DATE
        },
        is_verified: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return users;
}