module.exports = (db, Sequelize) => {
    const moneyTransaction = db.define('money_transactions', {
        money_transation_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        user_id: {
            type: Sequelize.STRING
        },
        song_release_id: {
            type: Sequelize.STRING
        },
        transaction_id: {
            type: Sequelize.STRING
        },
        transaction_type: {
            type: Sequelize.ENUM(['withdrawal', 'share purches'])
        },
        transaction_amount: {
            type: Sequelize.DECIMAL(15, 2)
        },
        commission_amount: {
            type: Sequelize.DECIMAL(15, 2)
        },
        transaction_status: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return moneyTransaction;
}