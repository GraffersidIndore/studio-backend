module.exports = (db, Sequelize) => {
    const songReleases = db.define('song_releases', {
        song_release_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        user_id: {
            type: Sequelize.STRING
        },
        artist_name: {
            type: Sequelize.STRING
        },
        artist_picture: {
            type: Sequelize.STRING
        },
        artist_description: {
            type: Sequelize.STRING
        },
        song_name: {
            type: Sequelize.STRING
        },
        song_release_date: {
            type: Sequelize.DATEONLY
        },
        record_label: {
            type: Sequelize.STRING
        },
        audio_file: {
            type: Sequelize.STRING
        },
        song_cover_picture: {
            type: Sequelize.STRING
        },
        song_total_earning: {
            type: Sequelize.DECIMAL(15, 2)
        },
        royalties_updated_time: {
            type: Sequelize.DATE
        },
        is_sale_active: {
            type: Sequelize.BOOLEAN
        },
        sell_sale_percentage: {
            type: Sequelize.INTEGER
        },
        sold_sale_percentage: {
            type: Sequelize.INTEGER
        },
        sale_price_per_percent: {
            type: Sequelize.DECIMAL(15, 2)
        },
        sale_exp_date: {
            type: Sequelize.DATEONLY
        },
        sale_description: {
            type: Sequelize.STRING
        },
        picture1: {
            type: Sequelize.STRING
        },
        picture2: {
            type: Sequelize.STRING
        },
        picture3: {
            type: Sequelize.STRING
        },
        picture4: {
            type: Sequelize.STRING
        },
        picture5: {
            type: Sequelize.STRING
        },
        picture6: {
            type: Sequelize.STRING
        },
        release_approved: {
            type: Sequelize.BOOLEAN
        },
        is_archived: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return songReleases;
}