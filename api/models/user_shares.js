module.exports = (db, Sequelize) => {
    const userShares = db.define('user_shares', {
        user_shares_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        user_id: {
            type: Sequelize.STRING
        },
        song_release_id: {
            type: Sequelize.STRING
        },
        sale_price_per_percent: {
            type: Sequelize.DECIMAL(15, 2)
        },
        sale_exp_date: {
            type: Sequelize.DATEONLY
        },
        sale_description: {
            type: Sequelize.STRING
        },
        purchased_percentage_sale: {
            type: Sequelize.INTEGER
        },
        total_purchased_price: {
            type: Sequelize.DECIMAL(15, 2)
        },
        user_share_earning: {
            type: Sequelize.DECIMAL(15, 2)
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return userShares;
}