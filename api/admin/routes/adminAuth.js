var express = require('express');
var router = express.Router();

const userAuthvalidator = require('../../middleware/validators/userAuthValidator')
const adminAuthController = require('../controllers/adminAuthController')
const auth = require('../../middleware/authentication')

// -> verify User Account
router.post('/login', userAuthvalidator.validate("login"),
    adminAuthController.login
);

// -> verify User Account
router.get('/logout', userAuthvalidator.validate("headers"), auth.isAdminAuthenticated,
    adminAuthController.logout
);

module.exports = router;