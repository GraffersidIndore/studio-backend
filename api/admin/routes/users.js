var express = require('express');
var router = express.Router();

const usersController = require('../controllers/usersController')
const auth = require('../../middleware/authentication')
const userAuthValidator = require('../../middleware/validators/userAuthValidator')

router.get('/get_all_users', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    usersController.getAllUsers
);


module.exports = router;

