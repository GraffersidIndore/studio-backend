var express = require('express');
var router = express.Router();

const releaseRequestsValidator = require('../../middleware/validators/releaseRequestsValidator');
const releaseRequestsController = require('../controllers/releaseRequestsController')
const auth = require('../../middleware/authentication')
const userAuthValidator = require('../../middleware/validators/userAuthValidator')

// -> verify User Account

router.get('/get_all_requests', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseRequestsController.getAllRequests
);

router.post('/approve_request', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseRequestsValidator.validate("ReleaseRequest"),
    releaseRequestsController.approveRequest
);

router.post('/reject_request', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseRequestsValidator.validate("ReleaseRequest"),
    releaseRequestsController.rejectRequest
);

router.post('/archive_request', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseRequestsValidator.validate("ReleaseRequest"),
    releaseRequestsController.archiveRequest
);

router.get('/get_all_archive_requests', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseRequestsController.getAllArchiveRequests
);

module.exports = router;