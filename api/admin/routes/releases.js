var express = require('express');
var router = express.Router();

const releasesController = require('../controllers/releasesController')
const auth = require('../../middleware/authentication')
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const releaseSongValidator = require("../../middleware/validators/releaseSongValidator")

router.get('/get_all_releases', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releasesController.getAllReleases
);

router.patch('/report_royalties', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    releaseSongValidator.validate("reportRoyalties"),
    releasesController.reportRoyalties
);

module.exports = router;