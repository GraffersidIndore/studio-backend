var express = require('express');
var router = express.Router();

const transactionsController = require('../controllers/transactionsController')
const auth = require('../../middleware/authentication')
const userAuthValidator = require('../../middleware/validators/userAuthValidator')

router.get('/get_all_transactions', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    transactionsController.getAllTransactions
);

module.exports = router;