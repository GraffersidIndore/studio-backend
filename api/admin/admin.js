var express = require('express');
var router = express.Router();

var adminAuth = require('./routes/adminAuth');
var releaseRequests = require('./routes/releaseRequests');
var transactions = require('./routes/transactions');
var releases = require("./routes/releases")
var users = require("./routes/users")
const auth = require('../middleware/authentication')
const userAuthValidator = require('../middleware/validators/userAuthValidator')
const overviewController = require("./controllers/overviewController")

// ->  User Authentication
router.use('/auth', adminAuth);

// ->  User Authentication
router.use('/release_request', releaseRequests);

// ->  transactions
router.use('/transaction', transactions);

// ->  Releases
router.use('/releases', releases);

// ->  Releases
router.use('/users', users);

router.get('/overview', userAuthValidator.validate("headers"), auth.isAdminAuthenticated,
    overviewController.getOverview
);


module.exports = router;