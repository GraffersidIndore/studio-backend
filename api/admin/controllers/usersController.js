const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const userDetailsService = require("../../services/userDetailsService");

exports.getAllUsers = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getAllUsers(req.query)
        .then(result => {
            res.status(200).send({ "users": result, "status": true, "message": "users fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/users/get_all_users -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}

exports.getOverview = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getOverview(req.query)
        .then(result => {
            res.status(200).send({ "users": result, "status": true, "message": "users fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/users/get_all_users -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}