const { validationResult } = require('express-validator');
const crypto = require('crypto');
const { logger } = require("../../../config/logger");
var bcrypt = require('../../lib/bcrypt');
var jwt = require('../../lib/jwt');

//require service
const releaseRequestsService = require('../../services/releaseRequestsService');


exports.getAllRequests = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseRequestsService.getAllRequests()
        .then(result => {
            res.status(200).send({ "releaseRequestCount": result.releaseRequestCount, "releaseRequests": result.releaseRequests, "status": true, "message": "Release requests fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/release_request/get_all_requests -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}

exports.approveRequest = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseRequestsService.approveRequest(req.body)
        .then(result => {
            if (result[0] == 1) {
                res.status(200).send({ "status": true, "message": "Requests approved successfully." });
            } else {
                res.status(400).send({ "status": false, "message": "Unable to approved the Requests." });
            }
        }).catch((error) => {
            logger.error('/admin/release_request/approve_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.rejectRequest = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseRequestsService.rejectRequest(req.body)
        .then(result => {
            if (result[0] == 1) {
                res.status(200).send({ "status": true, "message": "Rejected request successfully." });
            } else {
                res.status(400).send({ "status": false, "message": "Unable to reject the Requests." });
            }
        }).catch((error) => {
            logger.error('/admin/release_request/reject_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.archiveRequest = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseRequestsService.archiveRequest(req.body)
        .then(result => {
            if (result[0] == 1) {
                res.status(200).send({ "status": true, "message": "Archived request successfully." });
            } else {
                res.status(400).send({ "status": false, "message": "Unable to archive the Requests." });
            }
        }).catch((error) => {
            logger.error('/admin/release_request/approve_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getAllArchiveRequests = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseRequestsService.getAllArchiveRequests()
        .then(result => {
            res.status(200).send({ "archivedReleaseRequestCount": result.archivedReleaseRequestCount, "archivedReleaseRequests": result.archivedReleaseRequests, "status": true, "message": "Archived Release requests fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/release_request/get_all_archieve_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}