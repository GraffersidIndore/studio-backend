const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const releaseSongService = require("../../services/releaseSongService")

exports.getAllReleases = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseSongService.getAllReleasedSongs(req.query)
        .then(result => {
            res.status(200).send({ "releases": result, "status": true, "message": "Releases fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/releases/get_all_releases -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}

exports.reportRoyalties = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    releaseSongService.reportRoyalties(req.body)
        .then(result => {
            if (result == 1) {
                res.status(200).send({ "status": true, "message": "Reported royalties updated successfully." });
            } else {
                res.status(400).send({ "status": false, "message": "Reported royalties is not update." });
            }
        }).catch((error) => {
            logger.error('/admin/releases/get_all_releases -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}