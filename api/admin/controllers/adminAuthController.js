const { validationResult } = require('express-validator');
const crypto = require('crypto');
const { logger } = require("../../../config/logger");
var bcrypt = require('../../lib/bcrypt');
var jwt = require('../../lib/jwt');

//require service
const userAuthService = require('../../services/userAuthService');

exports.login = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { email, password } = req.body;

    userAuthService.login(email)
        .then(async user => {
            if (user !== null) {

                const { user_id, user_role } = user;
                if (user_role !== "admin") {
                    res.status(404).send({ status: false, message: "Account is not valid." });
                    return;
                }
                var passwordIsValid = bcrypt.comparePassword(user.password, password);//hashPassword,password
                if (passwordIsValid) {
                    jwt.generateToken(user_id, user_role).then((token) => {
                        userAuthService.setUserAccessToken(token, user_id)
                            .then(result => {
                                res.status(200).send({ "email": result.email, "firstname": result.firstname, "lastname": result.lastname, "profile_picture": result.profile_picture, "access_token": result.access_token, status: true, message: "Loggedin successfully" });
                            }).catch((error) => {
                                logger.error('/admin/auth/login -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: error })
                            })
                    });
                } else {
                    res.status(403).send({ status: false, message: "Entered wrong password" });
                }

            } else {
                res.status(404).send({ status: false, message: "User doesn't exist" });
            }

        }).catch((error) => {
            logger.error('/admin/auth/login -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Login User
exports.logout = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('admin/auth/logout -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userAuthService.logout(userId)
        .then(user => {
            if (user !== null) {
                res.status(200).send({ status: true, message: "Logout Successfully" });
            } else {
                res.status(401).send({ status: false, message: "Something went wrong." });
            }
        }).catch((error) => {
            logger.error('/admin/auth/logout -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}