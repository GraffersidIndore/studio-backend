const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const overviewServices = require("../../services/overviewService")

exports.getOverview = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    overviewServices.getOverview()
        .then(result => {
            res.status(200).send({ overview: result, "status": true, "message": "Overview data fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/overview -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}