const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const transactionService = require("../../services/transactionService")

exports.getAllTransactions = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    transactionService.getAllTransactions()
        .then(result => {
            res.status(200).send({ "transactions": result, "status": true, "message": "transactions fetched successfully." });
        }).catch((error) => {
            logger.error('/admin/transaction/get_all_transactions -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error });
        })

}